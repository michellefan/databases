package db;

/**
 * Created by michellefan on 2/28/17.
 */
public class Test {
    public static void main(String[] args) {
        String[] colNames1 = {"X int", "Y int"};
        String[] colNames2 = {"Ack int", "Baller int"};
        Database db = new Database();
        db.createNewTable("t1", colNames1);
        String[] row1 = {"1", "7"};
        String[] row2 = {"7", "7"};
        String[] row3 = {"1", "9"};
        db.insertRow("t1", row1);
        db.insertRow("t1", row2);
        db.insertRow("t1", row3);
        db.createNewTable("t2", colNames2);
        String[] row4 = {"3", "0"};
        String[] row5 = {"4", "0"};
        String[] row6 = {"5", "0"};
        db.insertRow("t2", row4);
        db.insertRow("t2", row5);
        db.insertRow("t2", row6);
        String[] colNames3 = {"Ack string", "Baller string"};
        db.createNewTable("t3", colNames3);
        String[] row7 = {"'ab'", "'cd'"};
        String[] row8 = {"'ef'", "'gh'"};
        String[] row9 = {"'ij'", "'kl'"};
        db.insertRow("t3", row7);
        db.insertRow("t3", row8);
        db.insertRow("t3", row9);
        String[] colNames4 = {"Lastname int" ,"Firstname int","TeamName string"};
        String[] row10 = {"'Lee'","'Maurice'","'Mets'"};
        String[] row11 = {"'Lee'","'Maurice'","'Steelers'"};
        String[] row12 = {"'Ray'","'Mitas'","'Patriots'"};
        String[] row13 = {"'Hwang'","'Alex'","'Cloud9'"};
        String[] row14 = {"'Rulison'","'Jared'","'EnVyUs'"};
        String[] row15 = {"'Fang'","'Vivian'","'Golden Bears'"};
        db.createNewTable("fans", colNames4);
        db.insertRow("fans", row10);
        db.insertRow("fans", row11);
        db.insertRow("fans", row12);
        db.insertRow("fans", row13);
        db.insertRow("fans", row14);
        db.insertRow("fans", row15);
        db.transact("create table t5 as select X, Y/Baller as Y from t1, t2");
        System.out.println(db.transact("select * from t5 where X < Y"));
        //System.out.println(db.transact("select A + B as C from t2 where C > 3"));
        //System.out.println(db.transact("select Ack, Baller from t2 where Ack < Baller"));
        //System.out.println(db.transact("select Ack, Baller as C from t3 where Ack < Baller"));
        //Table joined = db.join(db.getTable("t1"), db.getTable("t2"));
        //db.addTable(joined);
        //System.out.println(db.printTable("t1"));
    }
}
