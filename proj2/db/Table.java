package db;

import java.util.ArrayList;
/**
 * Created by yaliniprabha on 2/27/17.
 */
public class Table {

    //public static ArrayList<String> tableNames = new ArrayList<>();
    private String tableName;
    private ArrayList<Column> columns;

    Table(String tName) {
        tableName = tName;
    }

    Table(String tName, ArrayList<Column> colz) {
        tableName = tName;
        columns = colz;
    }

    public String createColumns(String[] colNames) {
        columns = new ArrayList<>();
        for (String x: colNames) {
            String message = addColumn(x.split(" "));
            if (message.equals("ERROR")) {
                return message;
            }
        }
        return "";
    }

    public String getTableName() {
        return this.tableName;
    }

    public int numberOfColumns() {
        return columns.size();
    }

    public int numberOfRows() {
        return columns.get(0).getLength();
    }


    public Column getColumn(String name) { //FIX!!!!!!!!! if null blocks
        for (Column x: columns) {
            if (x.getColName().equals(name)) {
                return x;
            }
        }
        return null;
    }

    public Column getColumn(int i) {
        return columns.get(i);
    }

    public ArrayList<Column> getColumns() {
        return columns;
    }

    public String addColumn(String[] nameTypeWithSpaces) {
        String[] nameType = new String[2];
        int ctr = 0;
        for (int i = 0; i < nameTypeWithSpaces.length; i++) {
            if (!(nameTypeWithSpaces[i].equals(""))) {
                nameType[ctr] = nameTypeWithSpaces[i];
                ctr++;
            }
        }
        String x = nameType[1];
        if (x == null) {
            return "ERROR";
        } else if (x.equals("int") || x.equals("string") || x.equals("float")) {
            Column newCol = new Column(nameType[0], nameType[1]);
            columns.add(newCol);
        } else {
            return "ERROR";
        }
        return "";
    }

    public String addRow(String[] values) {
        if (values.length != this.numberOfColumns()) {
            return "ERROR: wrong number of values";
        }
        for (int i = 0; i < values.length; i++) {
            //if value to be added is NOVALUE, type of column doesn't matter
            //parse to float or int if necessary
            if (values[i].equals("NOVALUE")) {
                Value x = new Value(values[i], columns.get(i).getType());
                columns.get(i).addElement(x);
            } else if (columns.get(i).getType().equals("int")) {
                try {
                    Value x = new Value(Integer.parseInt(values[i]), "int");
                    columns.get(i).addElement(x);
                } catch (NumberFormatException e) {
                    return "ERROR: Type of value " + i + " does not match "
                            + "Integer column";
                }
            } else if (columns.get(i).getType().equals("float")) {
                try {
                    Value x = new Value(Float.parseFloat(values[i]), "float");
                    columns.get(i).addElement(x);
                } catch (NumberFormatException e) {
                    return "ERROR: Type of value " + i + " does not match "
                            + "Float column";
                }
            } else if (columns.get(i).getType().equals("string")) {
                if ((values[i].charAt(0) == '\'')
                        && values[i].charAt(values[i].length() - 1) == '\'') {
                    columns.get(i).addElement(new Value(values[i], "string"));
                } else {
                    return "ERROR: Type of value " + i + " does not match "
                            + "String column";
                }
            }
        }
        return "";
    }

    public void addRow(ArrayList<Value> values) {
        for (int i = 0; i < values.size(); i += 1) {
            columns.get(i).addElement(values.get(i));
        }
    }

}
