package db;

/**
 * Created by michellefan on 3/1/17.
 */
public class Value {
    private Object item;
    private String type;

    Value(Object item, String type) {
        this.item = item;
        this.type = type;
    }

    public Object getItem() {
        return item;
    }

    public String getType() {
        return type;
    }

    public boolean equalTo(Object obj) {
        if (((Value) obj).getItem().equals(getItem())) {
            return true;
        }
        return false;
    }

    public String convertToString() {
        if (item.equals("NaN") || item.equals("NOVALUE")) {
            return item.toString();
        } else if (type.equals("float")) {
            return String.format("%.03f", item);
        } else {
            return item.toString();
        }
    }

    public int compare(String lit) {
        if (getItem().equals("NaN")) {
            return 1;
        }
        try {
            Integer.parseInt(lit);
            int x = Integer.parseInt(lit);
            int first = Integer.parseInt(getItem().toString());
            if (first < x) {
                return -1;
            } else if (first > x) {
                return 1;
            } else {
                return 0;
            }
        } catch (NumberFormatException e) {
            try {
                Float.parseFloat(lit);
                float x = Integer.parseInt(lit);
                float first = Float.parseFloat(getItem().toString());
                if (first < x) {
                    return -1;
                } else if (first > x) {
                    return 1;
                } else {
                    return 0;
                }
            } catch (NumberFormatException f) {
                return getItem().toString().compareTo(lit);
            }
        }
    }

    public int compare(Value v) {
        if (getItem().equals("NaN")) {
            if (v.getItem().equals("NaN")) {
                return 0;
            }
            return 1;
        } else if (v.getItem().equals("NaN")) {
            return -1;
        } else if (getType().equals("int")) {
            int x = Integer.parseInt(getItem().toString());
            if (v.getType().equals("float")) {
                float y = Float.parseFloat(v.getItem().toString());
                if (x < y) {
                    return -1;
                } else if (x > y) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                int y = Integer.parseInt(v.getItem().toString());
                if (x < y) {
                    return -1;
                } else if (x > y) {
                    return 1;
                } else {
                    return 0;
                }
            }
        } else if (getType().equals("float")) {
            float x = Float.parseFloat(getItem().toString());
            if (v.getType().equals("float")) {
                float y = Float.parseFloat(v.getItem().toString());
                if (x < y) {
                    return -1;
                } else if (x > y) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                int y = Integer.parseInt(v.getItem().toString());
                if (x < y) {
                    return -1;
                } else if (x > y) {
                    return 1;
                } else {
                    return 0;
                }
            }
        } else {
            return getItem().toString().compareTo(v.getItem().toString());
        }
    }

    private static Value noValueAdder(Value a , Value b) {
        String typeA = a.getType();
        String typeB = b.getType();
        String newType;
        Object ob;
        if (typeA.equals("float") || typeB.equals("float")) {
            newType = "float";
        } else if (typeA.equals("string")) {
            newType = "string";
        } else {
            newType = "int";
        }
        if (a.getItem().equals("NOVALUE") && b.getItem().equals("NOVALUE")) {
            return new Value("NOVALUE", newType);
        } else if (a.getItem().equals("NOVALUE")) {
            if (typeA.equals("float") && typeB.equals("float")) {
                ob = 0.0 + (Float) b.getItem();
            } else if (typeA.equals("float") && typeB.equals("int")) {
                ob = 0.0 + (Integer) b.getItem();
            } else if (typeA.equals("int") && typeB.equals("float")) {
                ob = 0 + (Float) b.getItem();
            } else if (typeA.equals("int") && typeB.equals("int")) {
                ob = 0 + (Integer) b.getItem();
            } else {
                ob = "" + b.convertToString();
            }
        } else {
            if (typeA.equals("float") && typeB.equals("float")) {
                ob = (Float) a.getItem() + 0.0;
            } else if (typeA.equals("float") && typeB.equals("int")) {
                ob = (Float) a.getItem() + 0;
            } else if (typeA.equals("int") && typeB.equals("float")) {
                ob =  (Integer) a.getItem() + 0.0;
            } else if (typeA.equals("int") && typeB.equals("int")) {
                ob =  (Integer) a.getItem() + 0;
            } else {
                ob = a.convertToString() + "";
            }
        }
        return new Value(ob, newType);
    }

    private static Value noValueAdderLit(Value a , String b, String bType) {
        String typeA = a.getType();
        String newType;
        Object ob;
        if (typeA.equals("float") || bType.equals("float")) {
            newType = "float";
        } else if (typeA.equals("string")) {
            newType = "string";
        } else {
            newType = "int";
        }
        if (typeA.equals("float") && bType.equals("float")) {
            ob = 0.0 + Float.parseFloat(b);
        } else if (typeA.equals("float") && bType.equals("int")) {
            ob = 0.0 + Integer.parseInt(b);
        } else if (typeA.equals("int") && bType.equals("float")) {
            ob = 0 + Float.parseFloat(b);
        } else if (typeA.equals("int") && bType.equals("int")) {
            ob = 0 + Integer.parseInt(b);
        } else {
            ob = "" + b;
        }
        return new Value(ob, newType);
    }

    private static Value noValueSubtractor(Value a, Value b) {
        String typeA = a.getType();
        String typeB = b.getType();
        String newType;
        Object ob;
        if (typeA.equals("float") || typeB.equals("float")) {
            newType = "float";
        } else {
            newType = "int";
        }
        if (a.getItem().equals("NOVALUE") && b.getItem().equals("NOVALUE")) {
            return new Value("NOVALUE", newType);
        } else if (a.getItem().equals("NOVALUE")) {
            if (typeA.equals("float") && typeB.equals("float")) {
                ob = (Float) b.getItem() - 0.0;
            } else if (typeA.equals("float") && typeB.equals("int")) {
                ob = (Integer) b.getItem() - 0.0;
            } else if (typeA.equals("int") && typeB.equals("float")) {
                ob = (Float) b.getItem() - 0;
            } else if (typeA.equals("int") && typeB.equals("int")) {
                ob = (Integer) b.getItem() - 0;
            } else {
                ob = "" + b.convertToString();
            }
        } else {
            if (typeA.equals("float") && typeB.equals("float")) {
                ob = (Float) a.getItem() - 0.0;
            } else if (typeA.equals("float") && typeB.equals("int")) {
                ob = (Float) a.getItem() - 0;
            } else if (typeA.equals("int") && typeB.equals("float")) {
                ob =  (Integer) a.getItem() - 0.0;
            } else if (typeA.equals("int") && typeB.equals("int")){
                ob =  (Integer) a.getItem() - 0;
            } else {
                ob = "" + a.convertToString();
            }
        }
        return new Value(ob, newType);
    }

    private static Value noValueSubtractorLit(Value a , String b, String bType) {
        String typeA = a.getType();
        String newType;
        Object ob;
        if (typeA.equals("float") || bType.equals("float")) {
            newType = "float";
        } else if (typeA.equals("string")) {
            newType = "string";
        } else {
            newType = "int";
        }
        if (typeA.equals("float") && bType.equals("float")) {
            ob = Float.parseFloat(b) - 0.0;
        } else if (typeA.equals("float") && bType.equals("int")) {
            ob = Integer.parseInt(b) - 0.0;
        } else if (typeA.equals("int") && bType.equals("float")) {
            ob = Float.parseFloat(b) - 0;
        } else if (typeA.equals("int") && bType.equals("int")) {
            ob = Integer.parseInt(b) - 0;
        } else {
            ob = "" + b;
        }
        return new Value(ob, newType);
    }

    private static Value noValueMultiplier(Value a, Value b) {
        String typeA = a.getType();
        String typeB = b.getType();
        String newType;
        Object ob;
        if (typeA.equals("float") || typeB.equals("float")) {
            newType = "float";
        } else {
            newType = "int";
        }
        if (a.getItem().equals("NOVALUE") && b.getItem().equals("NOVALUE")) {
            return new Value("NOVALUE", newType);
        } else if (a.getItem().equals("NOVALUE")) {
            if (typeA.equals("float") && typeB.equals("float")) {
                ob = (Float) b.getItem() * 0.0;
            } else if (typeA.equals("float") && typeB.equals("int")) {
                ob = (Integer) b.getItem() * 0.0;
            } else if (typeA.equals("int") && typeB.equals("float")) {
                ob = (Float) b.getItem() * 0;
            } else {
                ob = (Integer) b.getItem() * 0;
            }
        } else {
            if (typeA.equals("float") && typeB.equals("float")) {
                ob = (Float) a.getItem() * 0.0;
            } else if (typeA.equals("float") && typeB.equals("int")) {
                ob = (Float) a.getItem() * 0;
            } else if (typeA.equals("int") && typeB.equals("float")) {
                ob =  (Integer) a.getItem() * 0.0;
            } else  {
                ob =  (Integer) a.getItem() * 0;
            }
        }
        return new Value(ob, newType);
    }

    private static Value noValueMultiplierLit(Value a , String b, String bType) {
        String typeA = a.getType();
        String newType;
        Object ob;
        if (typeA.equals("float") || bType.equals("float")) {
            newType = "float";
        } else if (typeA.equals("string")) {
            newType = "string";
        } else {
            newType = "int";
        }
        if (typeA.equals("float") && bType.equals("float")) {
            ob = Float.parseFloat(b) * 0.0;
        } else if (typeA.equals("float") && bType.equals("int")) {
            ob = Integer.parseInt(b) * 0.0;
        } else if (typeA.equals("int") && bType.equals("float")) {
            ob = Float.parseFloat(b) * 0;
        } else if (typeA.equals("int") && bType.equals("int")) {
            ob = Integer.parseInt(b) * 0;
        } else {
            ob = "" + b;
        }
        return new Value(ob, newType);
    }

    private static Value noValueDivider(Value a, Value b) {
        String typeA = a.getType();
        String typeB = b.getType();
        String newType;
        Object ob;
        if (typeA.equals("float") || typeB.equals("float")) {
            newType = "float";
        } else {
            newType = "int";
        }
        if (a.getItem().equals("NOVALUE") && b.getItem().equals("NOVALUE")) {
            return new Value("NOVALUE", newType);
        }
        ob = "NaN";
        return new Value(ob, newType);
    }

    private static Value noValueDividerLit(Value a , String b, String bType) {
        String typeA = a.getType();
        String newType;
        Object ob;
        if (typeA.equals("float") || bType.equals("float")) {
            newType = "float";
        } else if (typeA.equals("string")) {
            newType = "string";
        } else {
            newType = "int";
        }
        ob = "NaN";
        return new Value(ob, newType);
    }

    public static Value add(Value a, Value b) {
        Value result;
        String newType;
        if (a.getType().equals("float") || b.getType().equals("float")) {
            newType = "float";
        } else if (a.getType().equals("string")) {
            newType = "string";
        } else {
            newType = "int";
        }
        if (a.getItem().equals("NaN")) {
            return new Value("NaN", newType);//doesn't always have to be A's type- needs to be changed.
        } else if (b.getItem().equals("NaN")) {
            return new Value("NaN", newType);
        } else if (a.getItem().equals("NOVALUE") || b.getItem().equals("NOVALUE")) {
            return noValueAdder(a, b);
        } else if (a.getType().equals("int") && b.getType().equals("int")) {
            Object x = ((Integer) b.getItem() + (Integer) a.getItem());
            result = new Value(x, "int");
            return result;
        } else if (a.getType().equals("float") && b.getType().equals("float")) {
            Object x = ((Float) b.getItem() + (Float) a.getItem());
            result = new Value(x, "float");
            return result;
        } else if (a.getType().equals("float") && b.getType().equals("int")) {
            Object x = ((Integer) b.getItem() + (Float) a.getItem());
            result = new Value(x, "float");
            return result;
        } else if (a.getType().equals("int") && b.getType().equals("float")) {
            Object x = ((Float) b.getItem() + (Integer) a.getItem());
            result = new Value(x, "float");
            return result;
        } else {
            String y = "'" + a.getItem().toString().replace("'", "")
                    + (b.getItem().toString().replace("'", "")) + "'";
            Object x = y;
            result = new Value(x, "string");
            return result;
        }
    }

    public static Value add(Value a, String b, String bType) {
        Value result;
        String newType;
        if (a.getType().equals("float") || bType.equals("float")) {
            newType = "float";
        } else if (a.getType().equals("string")) {
            newType = "string";
        } else {
            newType = "int";
        }
        if (a.getItem().equals("NaN")) {
            return new Value("NaN", newType);
        } else if (a.getItem().equals("NOVALUE")) {
            return noValueAdderLit(a, b, bType);
        } else if (a.getType().equals("int") && bType.equals("int")) {
            Object x = ((Integer) a.getItem() + Integer.parseInt(b));
            result = new Value(x, "int");
            return result;
        } else if (a.getType().equals("float") && bType.equals("float")) {
            Object x = ((Float) a.getItem() + Float.parseFloat(b));
            result = new Value(x, "float");
            return result;
        } else if (a.getType().equals("float") && bType.equals("int")) {
            Object x = ((Float) a.getItem() + Integer.parseInt(b));
            result = new Value(x, "float");
            return result;
        } else {
            Object x = ((Integer) a.getItem() + Float.parseFloat(b));
            result = new Value(x, "float");
            return result;
        }
    }


    public static Value subtract(Value a, Value b) {
        Value result;
        String newType;
        if (a.getType().equals("float") || b.getType().equals("float")) {
            newType = "float";
        } else {
            newType = "int";
        }
        if (a.getItem().equals("NaN") || (b.getItem().equals("NaN"))) {
            return new Value("NaN", newType);
        } else if (a.getItem().equals("NOVALUE") || b.getItem().equals("NOVALUE")) {
            return noValueSubtractor(a, b);
        } else if (a.getType().equals("int") && b.getType().equals("int")) {
            Object x = ((Integer) a.getItem() - (Integer) b.getItem());
            result = new Value(x, "int");
            return result;
        } else if (a.getType().equals("float") && b.getType().equals("float")) {
            Object x = ((Float) a.getItem() - (Float) b.getItem());
            result = new Value(x, "float");
            return result;
        } else if (a.getType().equals("float") && b.getType().equals("int")) {
            Object x = ((Float) a.getItem() - (Integer) b.getItem());
            result = new Value(x, "float");
            return result;
        } else {
            Object x = ((Integer) a.getItem() - (Float) b.getItem());
            result = new Value(x, "float");
            return result;
        }
    }

    public static Value subtract(Value a, String b, String bType) {
        Value result;
        String newType;
        if (a.getType().equals("float") || bType.equals("float")) {
            newType = "float";
        } else {
            newType = "int";
        }
        if (a.getItem().equals("NaN")) {
            return new Value("NaN", newType);
        } else if (a.getItem().equals("NOVALUE")) {
            return noValueSubtractorLit(a, b, bType);
        } else if (a.getType().equals("int") && bType.equals("int")) {
            Object x = ((Integer) a.getItem() - Integer.parseInt(b));
            result = new Value(x, "int");
            return result;
        } else if (a.getType().equals("float") && bType.equals("float")) {
            Object x = ((Float) a.getItem() - Float.parseFloat(b));
            result = new Value(x, "float");
            return result;
        } else if (a.getType().equals("float") && bType.equals("int")) {
            Object x = ((Float) a.getItem() - Integer.parseInt(b));
            result = new Value(x, "float");
            return result;
        } else {
            Object x = ((Integer) a.getItem() - Float.parseFloat(b));
            result = new Value(x, "float");
            return result;
        }
    }

    public static Value multiply(Value a, Value b) {
        Value result;
        String newType;
        if (a.getType().equals("float") || b.getType().equals("float")) {
            newType = "float";
        } else {
            newType = "int";
        }
        if (a.getItem().equals("NaN") || (b.getItem().equals("NaN"))) {
            return new Value("NaN", newType);
        } else if (a.getItem().equals("NOVALUE") || b.getItem().equals("NOVALUE")) {
            return noValueMultiplier(a, b);
        } else if (a.getType().equals("int") && b.getType().equals("int")) {
            Object x = ((Integer) b.getItem() * (Integer) a.getItem());
            result = new Value(x, "int");
            return result;
        } else if (a.getType().equals("float") && b.getType().equals("float")) {
            Object x = ((Float) b.getItem() * (Float) a.getItem());
            result = new Value(x, "float");
            return result;
        } else if (a.getType().equals("float") && b.getType().equals("int")) {
            Object x = ((Integer) b.getItem() * (Float) a.getItem());
            result = new Value(x, "float");
            return result;
        } else {
            Object x = ((Float) b.getItem() * (Integer) a.getItem());
            result = new Value(x, "float");
            return result;
        }
    }

    public static Value multiply(Value a, String b, String bType) {
        Value result;
        String newType;
        if (a.getType().equals("float") || bType.equals("float")) {
            newType = "float";
        } else {
            newType = "int";
        }
        if (a.getItem().equals("NaN")) {
            return new Value("NaN", newType);
        } else if (a.getItem().equals("NOVALUE")) {
            return noValueMultiplierLit(a, b, bType);
        } else if (a.getType().equals("int") && bType.equals("int")) {
            Object x = ((Integer) a.getItem() * Integer.parseInt(b));
            result = new Value(x, "int");
            return result;
        } else if (a.getType().equals("float") && bType.equals("float")) {
            Object x = ((Float) a.getItem() * Float.parseFloat(b));
            result = new Value(x, "float");
            return result;
        } else if (a.getType().equals("float") && bType.equals("int")) {
            Object x = ((Float) a.getItem() * Integer.parseInt(b));
            result = new Value(x, "float");
            return result;
        } else {
            Object x = ((Integer) a.getItem() * Float.parseFloat(b));
            result = new Value(x, "float");
            return result;
        }
    }

    public static Value divide(Value a, Value b) {
        Value result;
        String newType;
        if (a.getType().equals("float") || b.getType().equals("float")) {
            newType = "float";
        } else {
            newType = "int";
        }
        if (a.getItem().equals("NaN") || (b.getItem().equals("NaN"))) {
            return new Value("NaN", newType);
        } else if (a.getItem().equals("NOVALUE") || b.getItem().equals("NOVALUE")) {
            return noValueDivider(a, b);
        } else if (a.getType().equals("int") && b.getType().equals("int")) {
            try {
                Object x = ((Integer) a.getItem() / (Integer) b.getItem());
                result = new Value(x, "int");
                return result;
            } catch (ArithmeticException e) {
                return new Value("NaN", "int");
            }
        } else if (a.getType().equals("float") && b.getType().equals("float")) {
            Object x = ((Float) a.getItem() / (Float) b.getItem());
            if (((Float) x).isInfinite()) {
                result = new Value("NaN", "float");
            } else {
                result = new Value(x, "float");
            }
            return result;
        } else if (a.getType().equals("float") && b.getType().equals("int")) {
            try {
                Object x = ((Float) a.getItem() / (Integer) b.getItem());
                if (((Float) x).isInfinite()) {
                    result = new Value("NaN", "float");
                } else {
                    result = new Value(x, "float");
                }
                return result;
            } catch (ArithmeticException e) {
                return new Value("NaN", "float");
            }
        } else {
            Object x = ((Integer) a.getItem() / (Float) b.getItem());
            if (((Float) x).isInfinite()) {
                result = new Value("NaN", "float");
            } else {
                result = new Value(x, "float");
            }
            return result;
        }
    }

    public static Value divide(Value a, String b, String bType) {
        Value result;
        if (a.getItem().equals("NOVALUE")) {
            return noValueDividerLit(a, b, bType);
        } else if (bType.equals("int")) {
            Value val = new Value(Integer.parseInt(b), "int");
            return Value.divide(a, val);
        } else {
            Value val = new Value(Float.parseFloat(b), "float");
            return Value.divide(a, val);
        }
    }
}
