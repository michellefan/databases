package db;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileInputStream;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Database {

    private ArrayList<Table> tableList;

    public Database() {
        tableList = new ArrayList<>();
    }

    public void printTableList() { //used to test that tables being stored in database properly
        for (Table table : tableList) {
            System.out.println(table.getTableName());
        }
    }

    public ArrayList<Table> getTableList() {
        return tableList;
    }

    public String createNewTable(String name, String[] cols) {
        for (Table x: tableList) {
            if (name.equals(x.getTableName())) {
                return "ERROR: Table already exists in database";
            }
        }
        if (cols.length == 0) {
            return "ERROR: Table must have columns";
        } else {
            Table newTable = new Table(name);
            String message = newTable.createColumns(cols);
            if (message.equals("ERROR")) {
                return "ERROR: Invalid data type for column";
            }
            tableList.add(newTable);
            return "";
        }
    }

    public void addTable(Table t) {
        tableList.add(t);
    }

    public Table getTable(String name) {
        for (Table table : tableList) {
            if (table.getTableName().equals(name)) {
                return table;
            }
        }
        return null;
    }

    public String createSelectedTable(String name, String exprs, String tables, String conds) {
        for (Table x: tableList) {
            if (name.equals(x.getTableName())) {
                return "ERROR: Table already exists in database";
            }
        }
        Table temp = selectTable(exprs, tables, conds);
        if (temp == null) {
            return "ERROR: Invalid Table";
        }
        Table newTable = new Table(name, temp.getColumns());
        tableList.add(newTable);
        return "";
    }

    public String loadTable(String tableName) {
        try {
            Scanner input = new Scanner(new FileInputStream(tableName + ".tbl"));
            if (!input.hasNext()) {
                return "ERROR: invalid";
            }
            String[] nameTypes = input.nextLine().split(",");
            Table newTable = new Table(tableName);
            String message = newTable.createColumns(nameTypes);
            if (message.equals("ERROR")) {
                return "ERROR: Invalid data type for column";
            }
            while (input.hasNext()) {
                String[] rowValues = input.nextLine().split(",");
                message = newTable.addRow(rowValues);
                if (!message.equals("")) {
                    return message;
                }
            }
            Table toRemove = null;
            boolean mustRemove = false;
            for (Table table : tableList) {
                if (table.getTableName().equals(tableName)) {
                    mustRemove = true;
                    toRemove = table;
                }
            }
            if (mustRemove) {
                tableList.remove(toRemove);
            }
            tableList.add(newTable);
        } catch (FileNotFoundException e) {
            return "ERROR: File not found";
        }
        return "";
    }

    public String storeTable(String name) {
        try {
            FileWriter f = new FileWriter(name + ".tbl");
            BufferedWriter b = new BufferedWriter(f);
            b.write(printTable(name));
            if (b != null) {
                b.close();
            }
            return "";
        } catch (IOException e) {
            return "ERROR: filewriter error";
        }
    }

    public String dropTable(String name) {
        for (Table table: tableList) {
            if (table.getTableName().equals(name)) {
                tableList.remove(table);
                return "";
            }
        }
        return "ERROR: table " + name + " not found";
    }

    public String insertRow(String name, String[] values) {
        for (Table x: tableList) {
            if (name.equals(x.getTableName())) {
                return x.addRow(values);
            }
        }
        return "ERROR: Table not found";
    }

    public String printTable(String name) {
        Table table = getTable(name);
        if (table == null) {
            return "ERROR: Table does not exist in database";
        }
        String printed = printHelper(table);
        return printed;
    }

    private static String printHelper(Table table) {
        if (table == null) {
            return "ERROR: invalid table";
        }
        int counter = 0;
        String printed = "";
        for (int i = 0; i < table.numberOfColumns(); i += 1) {
            printed = printed + table.getColumn(i).getColName()
                    + " " + table.getColumn(i).getType();
            if (counter < table.numberOfColumns() - 1) {
                printed += ",";
            }
            counter++;
        }
        for (int i = 0; i < table.numberOfRows(); i++)  {
            counter = 0;
            printed += "\n";
            for (int j = 0; j < table.numberOfColumns(); j++) {
                printed = printed
                        + table.getColumn(j).getElement(i).convertToString();
                if (counter < table.numberOfColumns() - 1) {
                    printed += ",";
                }
                counter++;
            }
        }
        return printed;
    }

    public String select(String exprs, String tables, String conds) {
        String[] tablesToBeJoined = tables.split(",");
        for (int i = 0; i < tablesToBeJoined.length; i++) {
            tablesToBeJoined[i] = tablesToBeJoined[i].trim();
        }
        Table output;
        if (tablesToBeJoined.length == 0) {
            return "ERROR: Invalid number of tables in select statement";
        }
        if (tablesToBeJoined.length == 1) {
            output = getTable(tablesToBeJoined[0]);
        } else {
            output = join(getTable(tablesToBeJoined[0]), getTable(tablesToBeJoined[1]));
        }
        if (tablesToBeJoined.length > 2) {
            for (int i = 2; i < tablesToBeJoined.length; i += 1) {
                output = join(output, getTable(tablesToBeJoined[i]));
            }
        }
        if (exprs.equals("*")) {
            output = output;
        } else  {
            output = columnSelect(output, exprs);
        }
        if (conds ==  null) {
            return printHelper(output);
        } else {
            String[] actualConds = conds.split("and");
            for (String x: actualConds) {
                output = conditionSelect(output, x);
            }
            return printHelper(output);
        }
    }

    public Table selectTable(String exprs, String tables, String conds) {
        String[] tablesToBeJoined = tables.split(",");
        for (int i = 0; i < tablesToBeJoined.length; i++) {
            tablesToBeJoined[i] = tablesToBeJoined[i].trim();
        }
        Table output;
        if (tablesToBeJoined.length == 0) {
            return null;
        }
        if (tablesToBeJoined.length == 1) {
            output = getTable(tablesToBeJoined[0]);
        } else {
            output = join(getTable(tablesToBeJoined[0]), getTable(tablesToBeJoined[1]));
        }
        if (tablesToBeJoined.length > 2) {
            for (int i = 2; i < tablesToBeJoined.length; i += 1) {
                output = join(output, getTable(tablesToBeJoined[i]));
            }
        }
        if (exprs.equals("*")) {
            output = output;
        } else  {
            output = columnSelect(output, exprs);
        }
        if (conds ==  null) {
            return output;
        } else {
            String[] actualConds = conds.split("and");
            for (String x: actualConds) {
                output = conditionSelect(output, x);
            }
            return output;
        }
    }

    private Table conditionSelect(Table t, String cond) {
        Table output = new Table(t.getTableName());
        ArrayList<Column> newCols = new ArrayList<>();
        cond = cond.trim();
        String[] wordsAndSpaces = cond.split(" ");
        ArrayList<String> words = new ArrayList<>();
        int ctr = 0;
        for (int i = 0; i < wordsAndSpaces.length; i++) {
            if (!(wordsAndSpaces[i].equals(""))) {
                words.add(wordsAndSpaces[i]);
                ctr++;
            }
        }
        if (words.get(0).contains("<")) {
            words = operatorExtract(words, "<");
        } else if (words.get(0).contains(">")) {
            words = operatorExtract(words, ">");
        } else if (words.get(0).contains("<=")) {
            words = operatorExtract(words, "<=");
        } else if (words.get(0).contains(">=")) {
            words = operatorExtract(words, ">=");
        } else if (words.get(0).contains("==")) {
            words = operatorExtract(words, "==");
        } else if (words.get(0).contains("!=")) {
            words = operatorExtract(words, "==");
        }
        Column operand1 = t.getColumn(words.get(0));
        if (operand1 == null) {
            return null;
        }
        String operator = words.get(1);
        String operand2 = words.get(2);
        try {
            Integer.parseInt(operand2);
            if (!compareColumnTypes(operand1, "int", operator)) {
                return null;
            }
            return callCondOpFn(t, operand1, operand2, "int", operator);
        } catch (NumberFormatException e) {
            try {
                Float.parseFloat(operand2);
                if (!compareColumnTypes(operand1, "float", operator)) {
                    return null;
                }
                return callCondOpFn(t, operand1, operand2, "float", operator);
            } catch (NumberFormatException ex) {
                if ((operand2.charAt(0) == '\''
                        && operand2.charAt(operand2.length() - 1) == '\'')) {
                    if (!compareColumnTypes(operand1, "string", operator)) {
                        return null;
                    }
                    return callCondOpFn(t, operand1, operand2, "string", operator);
                }
                if (t.getColumn(operand2) == null) {
                    return null;
                }
                if (!compareColumnTypes(operand1, t.getColumn(operand2).getType(), operator)) {
                    return null;
                }
                return callCondOpFn(t, operand1, operand2, "column", operator);
            }
        }

    }

    private Table callCondOpFn(Table t, Column op1, String op2, String op2T, String operator) {
        ArrayList<Integer> rowsToBeAdded;
        if (op2T.equals("column")) {
            rowsToBeAdded = binary(op1, t.getColumn(op2), operator);
        } else {
            rowsToBeAdded = unary(op1, op2, operator);
        }
        ArrayList<Column> columns = new ArrayList<>();
        for (int i = 0; i < t.numberOfColumns(); i++) {
            Column col = new Column(t.getColumn(i).getColName(), t.getColumn(i).getType());
            for (int j = 0; j < t.numberOfRows(); j++) {
                if (rowsToBeAdded.contains(j)) {
                    col.addElement(t.getColumn(i).getElement(j));
                }
            }
            columns.add(col);
        }
        return new Table(t.getTableName(), columns);
    }

    private ArrayList<Integer> unary(Column a, String literal, String operator) {
        ArrayList<Integer> indicesToBeAdded = new ArrayList<>();
        if (operator.equals("==")) {
            for (int i = 0; i < a.getLength(); i++) {
                if (a.getElement(i).compare(literal) == 0) {
                    indicesToBeAdded.add(i);
                }
            }
        } else if (operator.equals("!=")) {
            for (int i = 0; i < a.getLength(); i++) {
                if (a.getElement(i).compare(literal) != 0) {
                    indicesToBeAdded.add(i);
                }
            }
        } else if (operator.equals("<")) {
            for (int i = 0; i < a.getLength(); i++) {
                if (a.getElement(i).compare(literal) < 0) {
                    indicesToBeAdded.add(i);
                }
            }
        } else if (operator.equals(">")) {
            for (int i = 0; i < a.getLength(); i++) {
                if (a.getElement(i).compare(literal) > 0) {
                    indicesToBeAdded.add(i);
                }
            }
        } else if (operator.equals("<=")) {
            for (int i = 0; i < a.getLength(); i++) {
                if (a.getElement(i).compare(literal) <= 0) {
                    indicesToBeAdded.add(i);
                }
            }
        } else {
            for (int i = 0; i < a.getLength(); i++) {
                if (a.getElement(i).compare(literal) >= 0) {
                    indicesToBeAdded.add(i);
                }
            }
        }
        return indicesToBeAdded;
    }

    private ArrayList<Integer> binary(Column a, Column b, String operator) {
        ArrayList<Integer> indicesToBeAdded = new ArrayList<>();
        if (operator.equals("==")) {
            for (int i = 0; i < a.getLength(); i++) {
                if (a.getElement(i).compare(b.getElement(i)) == 0) {
                    indicesToBeAdded.add(i);
                }
            }
        } else if (operator.equals("!=")) {
            for (int i = 0; i < a.getLength(); i++) {
                if (a.getElement(i).compare(b.getElement(i)) != 0) {
                    indicesToBeAdded.add(i);
                }
            }
        } else if (operator.equals("<")) {
            for (int i = 0; i < a.getLength(); i++) {
                if (a.getElement(i).compare(b.getElement(i)) < 0) {
                    indicesToBeAdded.add(i);
                }
            }
        } else if (operator.equals(">")) {
            for (int i = 0; i < a.getLength(); i++) {
                if (a.getElement(i).compare(b.getElement(i)) > 0) {
                    indicesToBeAdded.add(i);
                }
            }
        } else if (operator.equals("<=")) {
            for (int i = 0; i < a.getLength(); i++) {
                if (a.getElement(i).compare(b.getElement(i)) <= 0) {
                    indicesToBeAdded.add(i);
                }
            }
        } else {
            for (int i = 0; i < a.getLength(); i++) {
                if (a.getElement(i).compare(b.getElement(i)) >= 0) {
                    indicesToBeAdded.add(i);
                }
            }
        }
        return indicesToBeAdded;
    }


    public Table join(Table t1, Table t2) {
        /* joins tables t1 and t2 */
        ArrayList<String> sharedColumns = new ArrayList<>();
        for (int i = 0; i < t1.numberOfColumns(); i += 1) {
            for (int j = 0; j < t2.numberOfColumns(); j += 1) {
                if ((t1.getColumn(i).getColName().equals(t2.getColumn(j).getColName()))
                        && (t1.getColumn(i).getType().equals(t2.getColumn(j).getType()))) {
                    sharedColumns.add(t1.getColumn(i).getColName());
                }
            }
        }
        if (sharedColumns.isEmpty()) {
            return cartesianProduct(t1, t2);
        } else {
            return joinHelper(t1, t2, sharedColumns);
        }
    }

    private String[] createColumnList(Table t1, Table t2, ArrayList<String> sharedColumns) {
        /* returns a String array containing all the column names in the final joined table
        parameters: t1 and t2 are tables to be joined, sharedColumns is a String ArrayList
        containing names of shared columns only
         */
        String[] columnNames = new String[t1.numberOfColumns()
                + t2.numberOfColumns() - sharedColumns.size()];
        for (int i = 0; i < sharedColumns.size(); i++) {
            columnNames[i] = sharedColumns.get(i) + " "
                    + t1.getColumn(sharedColumns.get(i)).getType();
        }
        int counter = sharedColumns.size();
        for (int i = 0; i < t1.numberOfColumns(); i++) {
            if (!sharedColumns.contains(t1.getColumn(i).getColName())) {
                columnNames[counter] = t1.getColumn(i).getColName() + " "
                        + t1.getColumn(i).getType();
                counter++;
            }
        }
        for (int j = 0; j < t2.numberOfColumns(); j++) {
            if (!sharedColumns.contains(t2.getColumn(j).getColName())) {
                columnNames[counter] = t2.getColumn(j).getColName() + " "
                        + t2.getColumn(j).getType();
                counter++;
            }
        }
        return columnNames;
    }

    private boolean rowCompare(Table t1, Table t2, int i, int j, ArrayList<String> sharedColumns) {
        /* checks if the jth row of t2 matches the ith row of t1 based on shared columns */
        for (String sharedColName: sharedColumns) {
            Value x = t1.getColumn(sharedColName).getElement(i);
            Value y = t2.getColumn(sharedColName).getElement(j);
            if (!x.equalTo(y)) {
                return false;
            }
        }
        return true;
    }

    private Table joinHelper(Table t1, Table t2, ArrayList<String> sharedColumns) {
        /* joins tables T1 and T2 when they only have 1 shared column */
        String[] columnNamesAndTypes = createColumnList(t1, t2, sharedColumns);
        Table result = new Table("result");
        result.createColumns(columnNamesAndTypes);
        for (int i = 0; i < t1.numberOfRows(); i += 1) {
            for (int j = 0; j < t2.numberOfRows(); j += 1) {
                if (rowCompare(t1, t2, i, j, sharedColumns)) {
                    ArrayList<Value> tempRow = new ArrayList<>();
                    for (String sharedColName: sharedColumns) {
                        tempRow.add(t1.getColumn(sharedColName).getElement(i));
                    }
                    for (int k = 0; k < t1.numberOfColumns(); k++) {
                        if (!sharedColumns.contains(t1.getColumn(k).getColName())) {
                            tempRow.add(t1.getColumn(k).getElement(i));
                        }
                    }
                    for (int k = 0; k < t2.numberOfColumns(); k++) {
                        if (!sharedColumns.contains(t2.getColumn(k).getColName())) {
                            tempRow.add(t2.getColumn(k).getElement(j));
                        }
                    }
                    result.addRow(tempRow);
                }
            }
        }
        return result;
    }

    private Table cartesianProduct(Table t1, Table t2) {
        String[] columnNames = new String[t1.numberOfColumns() + t2.numberOfColumns()];
        for (int i = 0; i < columnNames.length; i++) {
            if (i < t1.numberOfColumns()) {
                columnNames[i] = t1.getColumn(i).getColName() + " "
                        + t1.getColumn(i).getType();
            } else {
                columnNames[i] = t2.getColumn(i - t1.numberOfColumns()).getColName() + " "
                        + t2.getColumn(i - t1.numberOfColumns()).getType();
            }
        }
        Table result = new Table("result");
        result.createColumns(columnNames);
        for (int r1 = 0; r1 < t1.numberOfRows(); r1++) {
            for (int r2 = 0; r2 < t2.numberOfRows(); r2++) {
                ArrayList<Value> row = new ArrayList<>();
                for (int c1 = 0; c1 < t1.numberOfColumns(); c1++) {
                    row.add(t1.getColumn(c1).getElement(r1));
                }
                for (int c2 = 0; c2 < t2.numberOfColumns(); c2++) {
                    row.add(t2.getColumn(c2).getElement(r2));
                }
                result.addRow(row);
            }
        }
        return result;
    }

    public Table columnSelect(Table t, String exprs) {
        String[] colExps = exprs.split(",");
        for (int i = 0; i < colExps.length; i++) {
            colExps[i] = colExps[i].trim();
        }
        ArrayList<Column> newCols = new ArrayList<>();
        for (String colExp: colExps) {
            Column temp = evalColumnExp(t, colExp);
            if (temp == null) {
                return null;
            }
            newCols.add(temp);
        }
        return new Table(t.getTableName(), newCols);
    }

    private ArrayList<String> operatorExtract(ArrayList<String> words, String operator) {
        int index = words.get(0).indexOf(operator);
        String toBeSplit = words.get(0);
        words.add(0, toBeSplit.substring(0, index));
        words.add(1, operator);
        words.add(2, toBeSplit.substring(index + operator.length()));
        words.remove(3);
        return words;
    }

    public Column evalColumnExp(Table t, String colExp) {
        String[] wordsAndSpaces = colExp.split(" ");
        ArrayList<String> words = new ArrayList<>();
        int ctr = 0;
        for (int i = 0; i < wordsAndSpaces.length; i++) {
            if (!(wordsAndSpaces[i].equals(""))) {
                words.add(wordsAndSpaces[i]);
                ctr++;
            }
        }
        if (words.get(0).contains("+") && words.get(0).length() > 1) {
            words = operatorExtract(words, "+");
        } else if (words.get(0).contains("-") && words.get(0).length() > 1) {
            words = operatorExtract(words, "-");
        } else if (words.get(0).contains("*") && words.get(0).length() > 1) {
            words = operatorExtract(words, "*");
        } else if (words.get(0).contains("/") && words.get(0).length() > 1) {
            words = operatorExtract(words, "/");
        }

        boolean hasAlias = false;
        String alias = "";
        if (words.size() == 1) {
            return t.getColumn(words.get(0));
        } else {
            if (words.contains("as")) {
                hasAlias = true;
                alias = words.get(words.size() - 1);
                words.remove(words.size() - 1);
                words.remove(words.size() - 1);
            }
        }
        if (words.size() == 1) {
            return t.getColumn(words.get(0));
        } else {
            Column operand1 = t.getColumn(words.get(0));
            String operator = words.get(1);
            String operand2 = words.get(2);

            String name = operand1.getColName();
            if (hasAlias) {
                name = alias;
            }
            try {
                Integer.parseInt(operand2);
                if (!compareColumnTypes(operand1, "int", operator)) {
                    return null;
                }
                return callOpFn(t, operand1, operand2, "int", operator, name);
            } catch (NumberFormatException e) {
                try {
                    Float.parseFloat(operand2);
                    if (!compareColumnTypes(operand1, "float", operator)) {
                        return null;
                    }
                    return callOpFn(t, operand1, operand2, "float", operator, name);
                } catch (NumberFormatException ex) {
                    if (!compareColumnTypes(operand1, t.getColumn(operand2).getType(), operator)) {
                        return null;
                    }
                    return callOpFn(t, operand1, operand2, "column", operator, name);
                }
            }
        }
    }

    private Column callOpFn(Table t, Column o1, String o2, String o2T, String op, String n) {
        String colType;
        if (o2T.equals("column")) {
            colType = newColumnType(o1, t.getColumn(o2).getType(), op);
        } else {
            colType = newColumnType(o1, o2T, op);
        }
        if (op.equals("+")) {
            if (o2T.equals("column")) {
                return addColumns(o1, t.getColumn(o2), n, colType);
            }
            return addLiteral(o1, o2, o2T, n, colType);
        }
        if (op.equals("*")) {
            if (o2T.equals("column")) {
                return multiplyColumns(o1, t.getColumn(o2), n, colType);
            }
            return mulLiteral(o1, o2, o2T, n, colType);
        }
        if (op.equals("-")) {
            if (o2T.equals("column")) {
                return subtractColumns(o1, t.getColumn(o2), n, colType);
            }
            return subLiteral(o1, o2, o2T, n, colType);
        }
        if (op.equals("/")) {
            if (o2T.equals("column")) {
                return divideColumns(o1, t.getColumn(o2), n, colType);
            }
            return divideLiteral(o1, o2, o2T, n, colType);
        }
        return null;
    }

    private boolean compareColumnTypes(Column a, String operand2Type, String operator) {
        if (a.getType().equals("string") && operand2Type.equals("string")) {
            if (operator.equals("-") || operator.equals("/") || operator.equals("*")) {
                return false;
            }
        } else if (a.getType().equals("string") && operand2Type.equals("int")) {
            return false;
        } else if (a.getType().equals("string") && operand2Type.equals("float")) {
            return false;
        } else if (operand2Type.equals("string") && a.getType().equals("int")) {
            return false;
        } else if (operand2Type.equals("string") && a.getType().equals("float")) {
            return false;
        }
        return true;
    }

    private String newColumnType(Column a, String operand2Type, String operator) {
        if (a.getType().equals("float") || operand2Type.equals("float")) {
            return "float";
        } else if (a.getType().equals("string")) {
            return "string";
        }
        return "int";
    }

    private Column addColumns(Column a, Column b, String alias, String type) {
        Column resultColumn = new Column(alias, type);
        for (int i = 0; i < a.getLength(); i++) {
            Value resVal = Value.add(a.getElement(i), (b.getElement(i)));
            resultColumn.addElement(resVal);
        }
        return resultColumn;
    }

    private Column subtractColumns(Column a, Column b, String alias, String type) {
        Column resultColumn = new Column(alias, type);
        for (int i = 0; i < a.getLength(); i++) {
            resultColumn.addElement(Value.subtract(a.getElement(i), (b.getElement(i))));
        }
        return resultColumn;
    }


    private Column multiplyColumns(Column a, Column b, String alias, String type) {
        Column resultColumn = new Column(alias, type);
        for (int i = 0; i < a.getLength(); i++) {
            resultColumn.addElement(Value.multiply(a.getElement(i), (b.getElement(i))));
        }
        return resultColumn;
    }

    private Column divideColumns(Column a, Column b, String alias, String type) {
        Column resultColumn = new Column(alias, type);
        for (int i = 0; i < a.getLength(); i++) {
            resultColumn.addElement(Value.divide(a.getElement(i), (b.getElement(i))));
        }
        return resultColumn;
    }

    private Column addLiteral(Column a, String op2, String op2Type, String alias, String type) {
        Column resultColumn = new Column(alias, type);
        for (int i = 0; i < a.getLength(); i++) {
            resultColumn.addElement(Value.add(a.getElement(i), op2, op2Type));
        }
        return resultColumn;
    }

    private Column subLiteral(Column a, String op2, String op2Type, String alias, String type) {
        Column resultColumn = new Column(alias, type);
        for (int i = 0; i < a.getLength(); i++) {
            resultColumn.addElement(Value.subtract(a.getElement(i), op2, op2Type));
        }
        return resultColumn;
    }

    private Column mulLiteral(Column a, String op2, String op2Type, String alias, String type) {
        Column resultColumn = new Column(alias, type);
        for (int i = 0; i < a.getLength(); i++) {
            resultColumn.addElement(Value.multiply(a.getElement(i), op2, op2Type));
        }
        return resultColumn;
    }

    private Column divideLiteral(Column a, String op2, String op2Type, String alias, String type) {
        Column resultColumn = new Column(alias, type);
        for (int i = 0; i < a.getLength(); i++) {
            resultColumn.addElement(Value.divide(a.getElement(i), op2, op2Type));
        }
        return resultColumn;
    }

    public String transact(String query) {
        Parser parser = new Parser(this);
        return parser.eval(query);
    }
}
