package db;

import java.util.ArrayList;

/**
 * Created by yaliniprabha on 2/27/17.
 */
public class Column {

    private String colName;
    private String type;
    private ArrayList<Value> contents;

    public Column(String name, String type) {
        contents = new ArrayList<>();
        colName = name;
        this.type = type;
    }

    public String getColName() {
        return colName;
    }

    public String getType() {
        return type;
    }

    public Value getElement(int i) {
        return contents.get(i);
    }

    public int getLength() {
        return contents.size();
    }

    public void addElement(Value element) {
        contents.add(element);
    }

}
